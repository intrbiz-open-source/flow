import z from 'zod';

import { identifierSchema } from './core';
import { dataCaptureSchema } from './capture';


export const flowChannelSchema = z.object({
    name: z.string(),
    country: z.string().regex(/[A-Z]{2}/),
    locale: z.string().regex(/[A-Z]{2}_[A-Z]{2}/),
});

export type FlowChannel = z.infer<typeof flowChannelSchema>;


export const flowClientSchema = z.object({
    name: z.string(),
    company: z.string().optional()
});

export type FlowClient = z.infer<typeof flowClientSchema>;


export const flowStartSchema = z.object({
    channel: flowChannelSchema,
    client: flowClientSchema.optional(),
    arguments: z.record(identifierSchema, z.any()).optional()
});

export type FlowStart = z.infer<typeof flowStartSchema>;


export const flowStateSchema = z.object({
    status: z.string(),
    currentStep: z.string().nullable(),
    currentSubstep: z.string().array()
});

export type FlowState = z.infer<typeof flowStateSchema>;


export const flowStepCaptureActionSchema = z.object({
    action: z.literal('CAPTURE'),
    capture: dataCaptureSchema
});

export type FlowStepCaptureAction = z.infer<typeof flowStepCaptureActionSchema>;


export const flowStepCompleteActionSchema = z.object({
    action: z.literal('COMPLETE'),
    result: z.record(identifierSchema, z.any())
});

export type FlowStepCompleteAction = z.infer<typeof flowStepCompleteActionSchema>;


export const flowStepActionSchema = z.discriminatedUnion('action', [
    flowStepCaptureActionSchema,
    flowStepCompleteActionSchema
]);

export type FlowStepAction = z.infer<typeof flowStepActionSchema>;


export const flowOutlineStepDisplaySchema = z.object({
    title: z.string(),
    intro: z.string().optional()
});

export type FlowOutlineStepDisplay = z.infer<typeof flowOutlineStepDisplaySchema>;


export const flowOutlineStepSchema = z.object({
    name: z.string(),
    active: z.boolean(),
    completed: z.boolean(),
    display: flowOutlineStepDisplaySchema
});

export type FlowOutlineStep = z.infer<typeof flowOutlineStepSchema>;


export const flowOutlineSchema = z.object({
    steps: flowOutlineStepSchema.array()
});

export type FlowOutline = z.infer<typeof flowOutlineSchema>;


export const flowStepSchema = z.object({
    executionId: z.string().uuid(),
    state: flowStateSchema,
    outline: flowOutlineSchema,
    action: flowStepActionSchema
});

export type FlowStep = z.infer<typeof flowStepSchema>;


export const flowSubmitSchema = z.object({
    executionId: z.string().uuid(),
    data: z.record(identifierSchema, z.any())
});

export type FlowSubmit = z.infer<typeof flowSubmitSchema>;
