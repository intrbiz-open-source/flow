import z, { ZodType } from 'zod';

import { identifierSchema, localeCodeSchema } from './core';
import { validationSchema } from './validation';


export const fieldDataTypeParsers: Readonly<Record<string, ZodType>> = {
    "string": z.string(), 
    "uuid": z.string().uuid(),
    "number": z.number(),
    "boolean": z.boolean(),
    "timestamp": z.string().datetime(),
    "localdate": z.string().regex(/[0-9]{4}-[0-9]{2}-[0-9]{2}/),
    "localtime": z.string().regex(/([0-9]{2}[:][0-9]{2})([:][0-9]{2})?/)
};

const [firstfieldDataType, ...otherfieldDataTypes] = Object.keys(fieldDataTypeParsers) as string[];
export const fieldDataTypeSchema = z.enum([firstfieldDataType, ...otherfieldDataTypes]);

export type FieldDataType = z.infer<typeof fieldDataTypeSchema>;


export const fieldDisplayTextSchema = z.object({
    title: z.string(),
    placeholder: z.string().optional()
});

export const fieldDisplaySchema = fieldDisplayTextSchema.extend({
    fieldType: z.string(),
    grouping: z.string().optional(),
    localisation: z.record(localeCodeSchema, fieldDisplayTextSchema).optional()
});

export type FieldDisplay = z.infer<typeof fieldDisplaySchema>;


export const fieldOptionSchema = z.object({
    title: z.string(),
    value: z.any(),
    condition: z.string().optional()
});

export type FieldOption = z.infer<typeof fieldOptionSchema>;


export const fieldOptionGroupSchema = z.object({
    title: z.string(),
    options: fieldOptionSchema.array(),
    name: z.string().optional(),
    condition: z.string().optional()
});

export type FieldOptionGroup = z.infer<typeof fieldOptionGroupSchema>;


export const fieldSchema = z.object({
    name: identifierSchema,
    dataType: fieldDataTypeSchema,
    value: z.any().optional(),
    display: fieldDisplaySchema,
    condition: z.string().optional(),
    optionGroups: fieldOptionGroupSchema.array().optional(),
    validation: validationSchema
});

export type Field = z.infer<typeof fieldSchema>;
