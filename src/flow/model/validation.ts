import z from 'zod';

import { identifierSchema, localeCodeSchema } from './core';

export const validationRuleTypeSchema = z.enum([ 'REGEX', 'EXPRESSION' ]);

export type ValidationRuleType = z.infer<typeof validationRuleTypeSchema>;

export const validationRuleTextSchema = z.object({
    failureMessage: z.string()
});

export const validationRuleSchema = validationRuleTextSchema.extend({
    type: validationRuleTypeSchema,
    expression: z.string(),
    expressionPreCondition: z.string().optional(),
    localisation: z.record(localeCodeSchema, validationRuleTextSchema).optional()
});

export type ValidationRule = z.infer<typeof validationRuleSchema>;

export const validationSchema = z.object({
    mandatory: z.boolean().optional(),
    minLength: z.number().positive().optional(),
    maxLength: z.number().positive().optional(),
    rules: validationRuleSchema.array().optional()
});

export type Validation = z.infer<typeof validationSchema>;


export const validationResultFieldErrorSchema = z.object({
    missing: z.boolean().optional(),
    badType: z.boolean().optional(),
    badOption: z.boolean().optional(),
    tooLong: z.boolean().optional(),
    tooShort: z.boolean().optional(),
    failureMessage: z.string().optional()
});

export type ValidationResultFieldError = z.infer<typeof validationResultFieldErrorSchema>;

export const validationResultErrorSchema = z.object({
    failureMessage: z.string()
});

export type ValidationResultError = z.infer<typeof validationResultErrorSchema>;

export const validationResultSchema = z.object({
    valid: z.boolean(),
    fieldErrors: z.record(identifierSchema, validationResultFieldErrorSchema),
    errors: validationResultErrorSchema.array()
});

export type ValidationResult = z.infer<typeof validationResultSchema>;
