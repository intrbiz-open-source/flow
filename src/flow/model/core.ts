import z from 'zod';


export const identifierSchema = z.string().regex(/[a-zA-Z_][a-zA-Z0-9_]*/);

export type Identifier = z.infer<typeof identifierSchema>;


export const countryCodeSchema = z.string().regex(/[A-Z]{2}/);

export type CountryCode = z.infer<typeof countryCodeSchema>;


export const localeCodeSchema = z.string().regex(/[A-Z]{2}_[A-Z]{2}/);

export type LocalCode = z.infer<typeof localeCodeSchema>;
