import z from 'zod';

import { identifierSchema } from './core';
import { flowChannelSchema, flowClientSchema } from './step';

export const flowStatusSchema = z.enum([ 'IN_PROGRESS', 'COMPLETE' ]);

export type FlowStatus = z.infer<typeof flowStatusSchema>;

export const flowExecutionStateSchema = z.object({
    executionId: z.string().uuid(),
    status: flowStatusSchema,
    currentStep: identifierSchema.nullable(),
    currentSubstep: identifierSchema.array(),
    channel: flowChannelSchema,
    client: flowClientSchema.optional(),
    arguments: z.record(identifierSchema, z.any()),
    captured: z.record(identifierSchema, z.any()),
    models: z.record(identifierSchema, z.any())
});

export type FlowExecutionState = z.infer<typeof flowExecutionStateSchema>;
