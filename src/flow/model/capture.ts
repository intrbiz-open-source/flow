import z from 'zod';

import { localeCodeSchema } from './core';
import { fieldSchema } from './field';
import { validationSchema, validationResultSchema } from './validation';

export const dataCaptureDisplayTextSchema = z.object({
    title: z.string(),
    intro: z.string().optional()
});

export const dataCaptureDisplaySchema = dataCaptureDisplayTextSchema.extend({
    localisation: z.record(localeCodeSchema, dataCaptureDisplayTextSchema).optional()
});

export type DataCaptureDisplay = z.infer<typeof dataCaptureDisplaySchema>;


export const dataCaptureSchema = z.object({
    display: dataCaptureDisplaySchema,
    fields: fieldSchema.array(),
    validation: validationSchema.optional(),
    validationResult: validationResultSchema.optional()
});

export type DataCapture = z.infer<typeof dataCaptureSchema>;
