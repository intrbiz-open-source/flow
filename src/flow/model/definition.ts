import z from 'zod';

import { identifierSchema, localeCodeSchema } from './core';
import { fieldSchema } from './field';
import { dataCaptureSchema } from './capture';
import { validationSchema } from './validation';


export const fieldBindingSchema = z.object({
    path: z.string()
});

export type FieldBinding = z.infer<typeof fieldBindingSchema>;


export const fieldDefinitionSchema = fieldSchema.extend({
    binding: fieldBindingSchema.optional()
});

export type FieldDefinition = z.infer<typeof fieldDefinitionSchema>;


export const dataCaptureDefinitionSchema = dataCaptureSchema.extend({
    fields: fieldDefinitionSchema.array()
});

export type DataCaptureDefinition = z.infer<typeof dataCaptureDefinitionSchema>;


export const flowStepBaseRuleDefinitionSchema = z.object({
    condition: z.string().optional(),
    comment: z.string().optional()
});

export const flowStepGotoRuleDefinitionSchema = flowStepBaseRuleDefinitionSchema.extend({
    goto: identifierSchema
});

export type FlowStepGotoRuleDefinition = z.infer<typeof flowStepGotoRuleDefinitionSchema>;

export const flowStepCompleteRuleDefinitionSchema = flowStepBaseRuleDefinitionSchema.extend({
    complete: z.boolean(),
    result: z.record(identifierSchema, z.string()).optional()
});

export type FlowStepCompleteRuleDefinition = z.infer<typeof flowStepCompleteRuleDefinitionSchema>;

export const flowStepRuleDefinitionSchema = z.union([
    flowStepGotoRuleDefinitionSchema, 
    flowStepCompleteRuleDefinitionSchema
]);

export type FlowStepRuleDefinition = z.infer<typeof flowStepRuleDefinitionSchema>;


export const flowStepTransitionDefinitionSchema = z.object({
    rules: flowStepRuleDefinitionSchema.array().nonempty()
});

export type FlowStepTransitionDefinition = z.infer<typeof flowStepTransitionDefinitionSchema>;


export const flowSubstepDefinitionSchema = z.object({
    name: identifierSchema,
    capture: dataCaptureDefinitionSchema.optional(),
    references: z.object({
        capture: z.string().optional()
    }).optional(),
    transition: flowStepTransitionDefinitionSchema
});

export type FlowSubstepDefinition = z.infer<typeof flowSubstepDefinitionSchema>;

export const flowStepDefinitionDisplayTextSchema = z.object({
    title: z.string(),
    intro: z.string().optional()
});

export const flowStepDefinitionDisplaySchema = flowStepDefinitionDisplayTextSchema.extend({
    localisation: z.record(localeCodeSchema, flowStepDefinitionDisplayTextSchema).optional()
});

export type FlowStepDefinitionDisplay = z.infer<typeof flowStepDefinitionDisplaySchema>;


export const flowStepDefinitionSchema = flowSubstepDefinitionSchema.extend({
    display: flowStepDefinitionDisplaySchema.optional(),
    steps: flowSubstepDefinitionSchema.array().optional(),
    initialStep: flowStepGotoRuleDefinitionSchema.array().optional()
});

export type FlowStepDefinition = z.infer<typeof flowStepDefinitionSchema>;


export const flowInitDefinitionSchema = z.object({
    arguments: z.object({
        name: identifierSchema,
        validation: validationSchema
    }).array().optional(),
    models: z.object({
        name: identifierSchema,
        type: z.string(),
        args: z.object({
            name: identifierSchema,
            expression: z.string()
        }).array().optional()
    }).array().optional()
});

export type FlowInitDefinition = z.infer<typeof flowInitDefinitionSchema>;


export const flowDefinitionSchema = z.object({
    name: identifierSchema,
    initialStep: flowStepGotoRuleDefinitionSchema.array().optional(),
    steps: flowStepDefinitionSchema.array(),
    init: flowInitDefinitionSchema
});

export type FlowDefinition = z.infer<typeof flowDefinitionSchema>;
