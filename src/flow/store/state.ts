import { FlowExecutionState } from '../model/state';

/**
 * Store and load flow exection states
 */
export interface FlowStateStore {

    /**
     * Store the given flow exection state
     * @param state 
     */
    storeState(state: FlowExecutionState): Promise<void>;

    /**
     * Load the state for the given exection id
     * @param executionId 
     */
    loadState(executionId: string): Promise<FlowExecutionState>;

}
