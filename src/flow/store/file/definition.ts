import { readFile } from 'fs/promises';

import { ZodType } from 'zod';

import { FlowDefinitionStore } from '../definition';
import { FlowDefinition, FlowStepDefinition, DataCaptureDefinition, FlowSubstepDefinition } from '../../model/definition';
import { flowDefinitionSchema, dataCaptureDefinitionSchema } from '../../model/definition';

export class FlowFileDefinitionStore implements FlowDefinitionStore {

    basePath: string;

    flowCache: { [key: string]: FlowDefinition};

    constructor(basePath: string) {
        this.basePath = basePath;
        this.flowCache = {};
    }

    private async loadDefinition<T>(name: string, parser: ZodType<T>): Promise<T> {
        const path = this.basePath + '/' + name + '.json';
        // load the file
        const content = await readFile(path, {encoding: 'utf-8'});
        // parse the file
        return parser.parse(JSON.parse(content));
    }

    async loadDataCaptureDefinition(name: string): Promise<DataCaptureDefinition> {
        try {
            return await this.loadDefinition(name, dataCaptureDefinitionSchema);
        } catch(e) {
            console.log('Error: ', e);
            throw new Error('Could not load data capture definition [' + name + ']');
        }
    }

    private async resolveSubstep(flow: FlowDefinition, substep: FlowSubstepDefinition) {
        // resolve substep references
        if (substep?.references) {
            if (substep.references.capture) {
                substep.capture = await this.loadDataCaptureDefinition(substep.references.capture);
            }
        }
    }

    private async resolveStep(flow: FlowDefinition, step: FlowStepDefinition) {
        // resolve step references
        if (step?.references) {
            if (step.references.capture) {
                step.capture = await this.loadDataCaptureDefinition(step.references.capture);
            }
        }
        // resolve sub steps
        if (step.steps) {
            for (const substep of step.steps) {
                await this.resolveSubstep(flow, substep);
            }
        }
    }

    private validateFlowSubstepDefinition(flow: FlowDefinition, step: FlowStepDefinition, substep: FlowSubstepDefinition) {
        // validate the transition rules
        if (substep.transition?.rules) {
            for (const rule of substep.transition?.rules) {
                if ('goto' in rule) {
                    const referenced = step.steps?.find((s) => s.name == rule.goto);
                    if (! referenced) {
                        throw new Error("The step [" + step.name + "] references [" + rule.goto + "] which does not exist");
                    }
                }
            }
        }
    }

    private validateFlowStepDefinition(flow: FlowDefinition, step: FlowStepDefinition) {
        // validate the transition rules
        if (step.transition?.rules) {
            for (const rule of step.transition?.rules) {
                if ('goto' in rule) {
                    const referenced = flow.steps.find((s) => s.name == rule.goto);
                    if (! referenced) {
                        throw new Error("The step [" + step.name + "] references [" + rule.goto + "] which does not exist");
                    }
                }
            }
        }
        // validate substeps
        if (step.steps) {
            // substep names are unique
            if ((new Set(step.steps.map((s) => s.name))).size != step.steps.length) {
                throw new Error("Substep names of step [" + step.name + "] are not unique");
            }
            // validate each substep
            for (const substep of step.steps) {
                this.validateFlowSubstepDefinition(flow, step, substep);
            }
        }
    }

    private validateFlowDefinition(flow: FlowDefinition) {
        // need at least one step
        if (flow.steps.length == 0) {
            throw new Error("A flow needs at least one step");
        }
        // step names are unique
        if ((new Set(flow.steps.map((s) => s.name))).size != flow.steps.length) {
            throw new Error("Flow step names are not unique");
        }
        // validate flow steps
        for (const step of flow.steps) {
            this.validateFlowStepDefinition(flow, step);
        }
    }

    async loadFlowDefinition(name: string): Promise<FlowDefinition> {
        try {
            // check the cache
            if (this.flowCache[name]) {
                return this.flowCache[name];
            }
            // load the def
            const flowDef = await this.loadDefinition(name, flowDefinitionSchema);
            // load any references
            for (const stepDef of flowDef.steps) {
                await this.resolveStep(flowDef, stepDef);
            }
            // validate the flow def
            this.validateFlowDefinition(flowDef);
            // cache
            this.flowCache[name] = flowDef;
            return flowDef;
        } catch (e) {
            console.log('Error: ', e);
            throw new Error('Could not load flow definition [' + name + ']');
        }
    }

}