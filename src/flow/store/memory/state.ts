import { FlowStateStore } from '../state'
import { flowExecutionStateSchema, FlowExecutionState } from '../../model/state';

export class FlowMemoryStateStore implements FlowStateStore {

    states: { [key: string]: string };

    constructor() {
        this.states = {};
    }

    async storeState(state: FlowExecutionState): Promise<void> {
        this.states[state.executionId] = JSON.stringify(state);
    }

    async loadState(executionId: string): Promise<FlowExecutionState> {
        const state = this.states[executionId];
        if (! state) {
            throw new Error('No such flow state [' + executionId + ']');
        }
        return flowExecutionStateSchema.parse(JSON.parse(state));
    }

}