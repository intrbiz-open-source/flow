
import { FlowDefinition, DataCaptureDefinition } from '../model/definition';

/**
 * Store and load flow definitions
 */
export interface FlowDefinitionStore {

    /**
     * Load a data capture definition
     * @param name
     */
    loadDataCaptureDefinition(name: string): Promise<DataCaptureDefinition>;

    /**
     * Load a dlow definition
     * @param name 
     */
    loadFlowDefinition(name: string): Promise<FlowDefinition>;

}
