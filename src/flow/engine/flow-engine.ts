
import { v4 as uuidv4 } from 'uuid';
import jexl from 'jexl';
import Mustache from 'mustache';

import { FlowStart, FlowStep, FlowOutline, FlowStepAction, FlowStepCaptureAction, FlowStepCompleteAction, FlowOutlineStep, FlowOutlineStepDisplay, FlowSubmit } from '../model/step';
import { FlowDefinition, FlowStepDefinition, FlowSubstepDefinition, FlowStepRuleDefinition, FlowStepDefinitionDisplay, FlowStepCompleteRuleDefinition } from '../model/definition';
import { DataCapture, DataCaptureDisplay } from '../model/capture';
import { Field, fieldDataTypeParsers, FieldDisplay } from '../model/field';
import { Validation, ValidationResult, ValidationRule } from '../model/validation';
import { FlowExecutionState } from '../model/state';

import { FlowDefinitionStore } from '../store/definition';
import { FlowStateStore } from '../store/state';

/**
 * A factory to create a model
 */
export type ModelFactory = (args: Record<string, any>) => any;

/**
 * Handle the execution of a flow
 */
export class FlowEngine {

    definitionStore: FlowDefinitionStore;

    stateStore: FlowStateStore;

    modelFactories: Record<string, { factory: ModelFactory }>;

    constructor(definitionStore: FlowDefinitionStore, stateStore: FlowStateStore) {
        this.definitionStore = definitionStore;
        this.stateStore = stateStore;
        this.modelFactories = {};
    }

    /**
     * Register a model type factory with this flow engine
     * @param type the type name
     * @param factory the factory to be invoked
     */
    registerModel(type: string, factory: ModelFactory) {
        this.modelFactories[type] = { factory: factory };
    }

    /**
     * Start the execution of the given flow
     * @param name the flow name
     * @param flowStart the starting data for this flow
     * @returns the next step to progress along the flow
     */
    async flowStart(name: string, flowStart: FlowStart): Promise<FlowStep> {
        // load the flow definition
        const flow = await this.definitionStore.loadFlowDefinition(name);
        if (! flow) {
            throw new Error('No such flow [' + name + ']');
        }
        // create the flow state
        const state: FlowExecutionState = {
            executionId: uuidv4(),
            status: 'IN_PROGRESS',
            currentStep: null,
            currentSubstep: [],
            channel: flowStart.channel,
            client: flowStart.client,
            arguments: {},
            captured: {},
            models: {}
        }
        // init state (decode arguments, register models, etc)
        await this.initFlowExecutionState(flow, flowStart, state);
        // load the initial step and possible substep
        const currentStep = await this.resolveInitialStepName(flow, state);
        const currentSubstep = (currentStep.steps) ? await this.resolveInitialSubstep(flow, currentStep, state) : undefined;
        const actualStep = (currentSubstep) ? currentSubstep : currentStep;
        // update the state with the current step
        state.currentStep = currentStep.name;
        if (currentSubstep) {
            state.currentSubstep = [ currentSubstep.name ];
        }
        // store the state
        this.stateStore.storeState(state);
        // return the next step action
        return {
            executionId: state.executionId,
            state: {
                status: state.status,
                currentStep: state.currentStep,
                currentSubstep: state.currentSubstep
            },
            action: await this.buildAction(flow, actualStep, undefined, state),
            outline: await this.buildFlowOutline(flow, currentStep, state)
        };
    }

    /**
     * Submit data to the current step of the flow
     * @param name the flow
     * @param data the data to submit
     */
    async flowSubmit(name: string, data: FlowSubmit): Promise<FlowStep> {
        // load the flow definition
        const flow = await this.definitionStore.loadFlowDefinition(name);
        if (! flow) {
            throw new Error('No such flow [' + name + ']');
        }
        // load the execution state
        const state = await this.stateStore.loadState(data.executionId);
        // load the current step
        if (! state.currentStep) {
            throw new Error('Invalid execution state: no currentStep');
        }
        const currentStep = this.resolveStep(flow, state.currentStep);
        const currentSubstep = (currentStep.steps && state.currentSubstep.length > 0) ? this.resolveSubstep(currentStep, state.currentSubstep[0]) : undefined;
        const actualStep = (currentSubstep) ? currentSubstep : currentStep;
        // decode the submitted data
        if (! actualStep.capture) {
            throw new Error('Cannot submit data to the current step');
        }
        const validationResult = await this.decodeStepCaptureData(flow, actualStep, state, data);
        if (validationResult.valid) {
            // bind validadata into models
            await this.bindStepCaptureData(flow, actualStep, state);
            // invoke the substep transition
            if (currentSubstep) {
                const subTransitionRule = await this.applyStepTransitionRules(flow, currentSubstep, state);
                if ('goto' in subTransitionRule) {
                    const nextSubstep = this.resolveSubstep(currentStep, subTransitionRule.goto);
                    // update the state with the target substep
                    state.currentSubstep[0] = nextSubstep.name;
                    // store updated state
                    this.stateStore.storeState(state);
                    // respond with the next step
                    return {
                        executionId: state.executionId,
                        state: {
                            status: state.status,
                            currentStep: state.currentStep,
                            currentSubstep: state.currentSubstep
                        },
                        action: await this.buildAction(flow, nextSubstep, undefined, state),
                        outline: await this.buildFlowOutline(flow, currentStep, state)
                    };
                } else if ('complete' in subTransitionRule) {
                    // fall through to the main step transition
                } else {
                    throw new Error('Unexpected substep transition rule')
                }
            }
            // invoke the step transition
            const transitionRule = await this.applyStepTransitionRules(flow, currentStep, state);
            if ('goto' in transitionRule) {
                // resolve the next step and any substep
                const nextStep = this.resolveStep(flow, transitionRule.goto);
                const nextSubstep = (nextStep.steps) ? await this.resolveInitialSubstep(flow, nextStep, state) : undefined;
                const nextActualStep = (nextSubstep) ? nextSubstep : nextStep;
                // update the state with the target step
                state.currentStep = nextStep.name;
                if (nextSubstep) {
                    state.currentSubstep = [ nextSubstep.name ];
                }
                // store updated state
                this.stateStore.storeState(state);
                // respond with the next step
                return {
                    executionId: state.executionId,
                    state: {
                        status: state.status,
                        currentStep: state.currentStep,
                        currentSubstep: state.currentSubstep
                    },
                    action: await this.buildAction(flow, nextActualStep, undefined, state),
                    outline: await this.buildFlowOutline(flow, nextStep, state)
                };
            } else if ('complete' in transitionRule) {
                // update the state
                state.status = 'COMPLETE';
                state.currentStep = null;
                state.currentSubstep = [];
                // store updated state
                this.stateStore.storeState(state);
                // response with the completed step
                return {
                    executionId: state.executionId,
                    state: {
                        status: state.status,
                        currentStep: state.currentStep,
                        currentSubstep: state.currentSubstep
                    },
                    action: await this.buildCompleteAction(flow, transitionRule, state),
                    outline: await this.buildFlowOutline(flow, undefined, state)
                };
            } else {
                throw new Error('Unexpected transition rule');
            }
        } else {
            // TODO: do we need to store updated state?
            this.stateStore.storeState(state);
            // respond with the validation errors
            return {
                executionId: state.executionId,
                state: {
                    status: state.status,
                    currentStep: state.currentStep,
                    currentSubstep: state.currentSubstep
                },
                action: await this.buildAction(flow, actualStep, validationResult, state),
                outline: await this.buildFlowOutline(flow, currentStep, state)
            };
        }
    }

    private async applyStepTransitionRules(flow: FlowDefinition, step: FlowSubstepDefinition, state: FlowExecutionState): Promise<FlowStepRuleDefinition> {
        // evaluate which transition rule applies
        for (const rule of step.transition.rules) {
            if (rule.condition) {
                const result = await jexl.eval(rule.condition, state)
                if (result === true) {
                    return rule;
                }
            } else {
                return rule;
            }
        }
        throw new Error('Step failed to transition');
    }

    private async bindStepCaptureData(flow: FlowDefinition, step: FlowSubstepDefinition, state: FlowExecutionState): Promise<void> {
        if (! step.capture) {
            throw new Error('Cannot decode capture data for a step without a capture definition');
        }
        // bind decoded data into models
        for (const field of step.capture.fields) {
            if (field.binding) {
                const input = state.captured[field.name];
                // TODO: find a better solution for this
                // we only support simple bind expressions for now
                const path = field.binding.path.split(/[.[]/).filter(e => e !== '$');
                if (path.length > 0) {
                    // lookup the container
                    let container: any = state.models;
                    for (let i = 0; i < (path.length - 1); i++) {
                        const element = path[i];
                        let next: any = (element.endsWith(']')) ? container[ parseInt(element.substring(0, element.length - 1)) ] : container[element]; 
                        // create the container if needed
                        if (! next) {
                            next = (path[i + 1].endsWith(']')) ? [] : {};
                            if ((element.endsWith(']'))) {
                                container[ parseInt(element.substring(0, element.length - 1)) ] = next;
                            } else {
                                container[element] = next;
                            }
                        }
                        container = next;
                    }
                    // bind the value
                    const name = path[path.length - 1];
                    if (name.endsWith(']')) {
                        // bind array
                        const index = name.substring(0, name.length - 1);
                        if (index === '+') {
                            if (input) {
                                container.push(input);
                            }
                        } else {
                            if (input) {
                                container[ parseInt(index) ] = input;
                            } else {
                                container.splice(parseInt(index), 1);
                            }
                        }
                    } else {
                        // bind property
                        container[name] = input;
                    }
                }
            }
        }
        // reset captured data
        state.captured = {};
    }

    private async decodeStepCaptureData(flow: FlowDefinition, step: FlowSubstepDefinition, state: FlowExecutionState, data: FlowSubmit): Promise<ValidationResult> {
        if (! step.capture) {
            throw new Error('Cannot decode capture data for a step without a capture definition');
        }
        const result: ValidationResult = {
            valid: true,
            fieldErrors: {},
            errors: []
        };
        // decode the fields
        fields: for (const field of step.capture?.fields) {
            const input = data.data[field.name];
            // apply basic validation
            if (! input) {
                if (field.validation.mandatory) {
                    result.valid = false;
                    result.fieldErrors[field.name] = { missing: true };
                }
                continue fields;
            }
            // validate the input data type
            const parsedInput = fieldDataTypeParsers[field.dataType].safeParse(input);
            if (! parsedInput.success) {
                result.valid = false;
                result.fieldErrors[field.name] = { badType: true };
                continue fields;
            }
            const convertedInput = parsedInput.data;
            // minLength
            if (field.validation.minLength && typeof convertedInput === 'string' && convertedInput.length < field.validation.minLength) {
                result.valid = false;
                result.fieldErrors[field.name] = { tooShort: true };
                continue fields;
            }
            // maxLength
            if (field.validation.maxLength && typeof convertedInput === 'string' && convertedInput.length > field.validation.maxLength) {
                result.valid = false;
                result.fieldErrors[field.name] = { tooLong: true };
                continue fields;
            }
            // decode options
            if (field.optionGroups) {
                // TODO: validate option conditions
                let validOption = false;
                for (const optionGroup of field.optionGroups) {
                    for (const option of optionGroup.options) {
                        if (option.value === convertedInput) {
                            validOption = true;
                            break;
                        }
                    }
                }
                if (! validOption) {
                    result.valid = false;
                    result.fieldErrors[field.name] = { badOption: true };
                    continue fields;
                }
            }
            // apply rules
            if (field.validation.rules) {
                for (const rule of field.validation.rules) {
                    const ruleResult = await this.applyFieldValidationRule(field, rule, state, input);
                    if (! ruleResult) {
                        result.valid = false;
                        result.fieldErrors[field.name] = { 
                            failureMessage: this.localiseValidationRuleFailureMessage(rule, state)
                        };
                        continue fields;
                    }
                }
            }
            // store the captured data
            state.captured[field.name] = convertedInput;
        }
        // apply the whole capture validation
        if (step.capture.validation && step.capture.validation.rules) {
            for (const rule of step.capture.validation.rules) {
                const ruleResult = await this.applyCaptureValidationRule(rule, state);
                if (! ruleResult) {
                    result.valid = false;
                    result.errors.push({
                        failureMessage: this.localiseValidationRuleFailureMessage(rule, state)
                    });
                }
            }
        }
        return result;
    }

    private async applyCaptureValidationRule(rule: ValidationRule, state: FlowExecutionState): Promise<boolean> {
        if (rule.type === 'EXPRESSION') {
            // build expression context
            const context = { ...state.captured };
            // apply any precondition
            const preconditionResult = (rule.expressionPreCondition) ? ! (await jexl.eval(rule.expressionPreCondition, context) === false) : true;
            // apply the validation
            if (preconditionResult) {
                return await jexl.eval(rule.expression, context) === true;
            }
        }
        return true;
    }

    private async applyFieldValidationRule(field: Field, rule: ValidationRule, state: FlowExecutionState, input: any): Promise<boolean> {
        if (rule.type === 'REGEX') {
            // regex validation require a string input
            if (typeof input !== 'string') {
                return false;
            }
            // apply the pattern
            const pattern = new RegExp(rule.expression);
            return pattern.test(input);
        } else if (rule.type === 'EXPRESSION') {
            // build expression context
            const context = { ...state.captured, [field.name]: input };
            // apply any precondition
            const preconditionResult = (rule.expressionPreCondition) ? ! (await jexl.eval(rule.expressionPreCondition, context) === false) : true;
            // apply the validation
            if (preconditionResult) {
                return await jexl.eval(rule.expression, context) === true;
            }
        }
        return true;
    }

    private localiseValidationRuleFailureMessage(rule: ValidationRule, state: FlowExecutionState): string {
        // localise the message
        const localised = (rule.localisation && rule.localisation[state.channel.locale]) ? rule.localisation[state.channel.locale] : undefined;
        return this.renderTemplatedText(localised?.failureMessage, rule.failureMessage, state);
    }

    private async initFlowExecutionState(flow: FlowDefinition, flowStart: FlowStart, state: FlowExecutionState): Promise<void> {
        // init arguments
        if (flow.init.arguments) {
            for (const argument of flow.init.arguments) {
                const value = (flowStart.arguments) ? flowStart.arguments[argument.name] : undefined;
                // basic argument validation
                if (argument.validation.mandatory && ! value) {
                    throw new Error('The argument [' + argument.name + ']');
                }
                // TODO: extended argument validation
                // store the argument
                state.arguments[argument.name] = value;
            }
        }
        // init models
        if (flow.init.models) {
            for (const model of flow.init.models) {
                // extract any params of the model
                const args: Record<string, any> = {};
                if (model.args) {
                    for (const arg of model.args) {
                        args[arg.name] = await jexl.eval(arg.expression, state);
                    }
                }
                // do we have a factory for the model
                const factory = this.modelFactories[model.name];
                if (factory) {
                    // invoke the factory
                    state.models[model.name] = factory.factory(args);
                } else {
                    // default to create object from the args
                    state.models[model.name] = { ...args };
                }
            }
        }
    }

    private async buildAction(flow: FlowDefinition, currentStep: FlowSubstepDefinition, validationResult: ValidationResult | undefined, state: FlowExecutionState): Promise<FlowStepAction> {
        if (currentStep.capture) {
            return this.buildCaptureAction(flow, currentStep, currentStep.capture, validationResult, state);
        }
        throw new Error('Cannot build action for step [' + currentStep.name + ']');
    }

    private async buildCaptureAction(flow: FlowDefinition, currentStep: FlowSubstepDefinition, capture: DataCapture, validationResult: ValidationResult | undefined, state: FlowExecutionState): Promise<FlowStepCaptureAction> {
        return {
            action: 'CAPTURE',
            capture: { 
                display: this.buildCaptureDisplay(capture.display, state),
                fields: capture.fields.map((field) => {
                    return {
                        name: field.name,
                        dataType: field.dataType,
                        display: this.buildFieldCaptureDisplay(field.display, state),
                        value: state.captured[field.name],
                        condition: field.condition,
                        optionGroups: field.optionGroups,
                        validation: this.buildValidation(field.validation, state)
                    } as Field;
                }),
                validation: this.buildValidation(capture.validation, state),
                validationResult
            }
        };
    }

    private buildCaptureDisplay(display: DataCaptureDisplay, state: FlowExecutionState): DataCaptureDisplay {
        const localised = (display.localisation) ? display.localisation[state.channel.locale] : undefined;
        return {
            title: this.renderTemplatedText(localised?.title, display.title, state),
            intro: this.renderTemplatedTextNullable(localised?.intro, display.intro, state)
        }
    }

    private buildFieldCaptureDisplay(display: FieldDisplay, state: FlowExecutionState): FieldDisplay {
        const localised = (display.localisation) ? display.localisation[state.channel.locale] : undefined;
        return {
            fieldType: display.fieldType,
            title: this.renderTemplatedText(localised?.title, display.title, state),
            placeholder: this.renderTemplatedTextNullable(localised?.placeholder, display.placeholder, state),
            grouping: display.grouping
        }
    }

    private buildValidation(validation: Validation | undefined, state: FlowExecutionState): Validation | undefined {
        if (validation) {
            return {
                ...validation,
                rules: validation.rules?.map((rule) => {
                    const localised = (rule.localisation && rule.localisation[state.channel.locale]) ? rule.localisation[state.channel.locale] : undefined;
                    return {
                        type: rule.type,
                        expression: rule.expression,
                        expressionPreCondition: rule.expressionPreCondition,
                        failureMessage: this.renderTemplatedText(localised?.failureMessage, rule.failureMessage, state)
                    }
                })
            }
        }
        return undefined;
    }

    private async buildCompleteAction(flow: FlowDefinition, rule: FlowStepCompleteRuleDefinition, state: FlowExecutionState): Promise<FlowStepCompleteAction> {
        const result: Record<string, any> = {};
        // build result data
        if (rule.result) {
            for (const key in rule.result) {
                result[key] = await jexl.eval(rule.result[key], state.models);
            }
        }
        // return the complete action
        return {
            action: 'COMPLETE',
            result: result
        };
    }

    private async buildFlowOutline(flow: FlowDefinition, currentStep: FlowSubstepDefinition | undefined, state: FlowExecutionState): Promise<FlowOutline> {
        const steps: FlowOutlineStep[] = [];
        const currentStepIndex = (state.status == 'COMPLETE') ? flow.steps.length : flow.steps.findIndex((s) => s.name === currentStep?.name);
        // build the outline of the steps
        for (let i = 0; i < flow.steps.length; i++) {
            const step = flow.steps[i];
            steps.push({
                name: step.name,
                display: this.resolveStepOutlineDisplay(step, state),
                active: (i == currentStepIndex),
                completed: (i < currentStepIndex)
            });
        }
        return {
            steps: steps
        };
    }

    private resolveStepOutlineDisplay(step: FlowStepDefinition, state: FlowExecutionState): FlowOutlineStepDisplay {
        if (step.display) {
            return this.buildStepDisplay(step.display, state);
        } else if (step.capture) {
            return this.buildCaptureDisplay(step.capture.display, state);
        }
        // fallback to step name
        return {
            title: step.name
        };
    }

    private buildStepDisplay(display: FlowStepDefinitionDisplay, state: FlowExecutionState): FlowStepDefinitionDisplay {
        const localised = (display.localisation) ? display.localisation[state.channel.locale] : undefined;
        return {
            title: this.renderTemplatedText(localised?.title, display.title, state),
            intro: this.renderTemplatedTextNullable(localised?.intro, display.intro, state)
        }
    }

    private resolveStep(flow: FlowDefinition, name: string): FlowStepDefinition {
        const step = flow.steps.find((s) => s.name === name);
        if (! step) {
            throw new Error('Could not resolve step [' + name + ']');
        }
        return step;
    }

    private async resolveInitialStepName(flow: FlowDefinition, state: FlowExecutionState): Promise<FlowStepDefinition> {
        // apply the rules
        if (flow.initialStep) {
            for (const rule of flow.initialStep) {
                if (rule.condition) {
                    const result = await jexl.eval(rule.condition, state);
                    // if the condition was not true, we skip to the next rule
                    if (! (result === true)) {
                        continue;
                    }
                }
                // the rule matched as thre was either no condition or the condition matched
                return this.resolveStep(flow, rule.goto);
            }
        }
        // default to the first step
        return flow.steps[0];
    }

    private resolveSubstep(step: FlowStepDefinition, name: string): FlowSubstepDefinition {
        const substep = step.steps?.find((s) => s.name === name);
        if (! substep) {
            throw new Error('Could not resolve substep [' + name + '] within [' + step.name + ']');
        }
        return substep;
    }

    private async resolveInitialSubstep(flow: FlowDefinition, step: FlowStepDefinition, state: FlowExecutionState): Promise<FlowSubstepDefinition> {
        if (! step.steps) {
            throw new Error("Cannot resolve substep of a step which does not have substeps");
        }
        // default to the first substep
        return step.steps[0];
    }

    private renderTemplatedTextNullable(localisedText: string | undefined, defaultText: string | undefined, state: FlowExecutionState): string | undefined {
        const text = localisedText ? localisedText : defaultText;
        if (text) {
            return Mustache.render(text, state.models);
        }
        return undefined;
    }

    private renderTemplatedText(localisedText: string | undefined, defaultText: string, state: FlowExecutionState): string {
        return Mustache.render(localisedText ? localisedText : defaultText, state.models);
    }

}
