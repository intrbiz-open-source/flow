
export interface ClaimClient {
    channel: string;
    name: string;
    company?: string;
}

export interface ClaimType {
    name: string;
    supply: string;
    typeCode: string;
}

export class Plan {
    id: string;
    schemeCode: string;
    heatingCoverType?: string;
}

export interface Address {
    line1: string;
    line2?: string;
    city: string;
    area?: string
    postcode: string;
    information?: string;
}

export interface Contact {
    firstName: string;
    lastName: string;
    phone: string;
    email: string;
}

export interface Appliance {
    oemGroup?: String;
    oem: string;
    productTypeGroup?: string;
    productType: string;
    model: string;
    serial?: string;
}

export type ClaimStatus = 'INITIAL' | 'IN_PROGRESS' | 'ACCEPTED' | 'REJECTED' | 'COMPLETE';

export interface Claim {
    id: string;
    status: ClaimStatus;
    step: string;
    substep?: string;
    client: ClaimClient;
    claimType: ClaimType;
    plan: Plan;
    appliance: Appliance;
    contact: Contact;
    address: Address;
}
 
