import { FlowFileDefinitionStore } from '../flow/store/file/definition';
import { FlowMemoryStateStore } from '../flow/store/memory/state';
import { FlowEngine } from '../flow/engine/flow-engine';


const definitionStore = new FlowFileDefinitionStore('cfg/flows');
const stateStore = new FlowMemoryStateStore();
const flowEngine = new FlowEngine(definitionStore, stateStore);


async function loadDefinition() {
    // load out example definition
    const start = new Date();
    const flowDef = await definitionStore.loadFlowDefinition('claim/breakdown');
    const end = new Date();
    console.log('Flow Definition: ', JSON.stringify(flowDef, null, 2));
    console.log('Loaded flow definition: ' + (end.getTime() - start.getTime()) + 'ms');
}

async function main() {

    console.log('Starting flow [claim/breakdown]');
    const action1 = await flowEngine.flowStart('claim/breakdown', {
        channel: {
            name: 'example',
            country: 'GB',
            locale: 'EN_GB'
        },
        arguments: {
            planId: 'AG12425232'
        }
    });
    console.log('Action 1: ', JSON.stringify(action1, null, 2));
    console.log("State: ", JSON.stringify(await stateStore.loadState(action1.executionId), null, 2));

    const action2 = await flowEngine.flowSubmit('claim/breakdown', {
        executionId: action1.executionId,
        data: {
            "first_name": "Steven",
            "last_name": "Wilson",
            "email_address": "steve@example.com",
            "address_line_1": "1 Some Street",
            "city": "Kingston",
            "post_code": "KT1 2FH"
        }
    });
    console.log('Action 2: ', JSON.stringify(action2, null, 2));
    console.log("State: ", JSON.stringify(await stateStore.loadState(action2.executionId), null, 2));

    const action3 = await flowEngine.flowSubmit('claim/breakdown', {
        executionId: action2.executionId,
        data: {
            "appliance_oem": "whirlpool",
        }
    });
    console.log('Action 3: ', JSON.stringify(action3, null, 2));
    console.log("State: ", JSON.stringify(await stateStore.loadState(action3.executionId), null, 2));

    const action4 = await flowEngine.flowSubmit('claim/breakdown', {
        executionId: action3.executionId,
        data: {
            "fault_description": "There is a loud knocking sound when the washing machine spins.",
        }
    });
    console.log('Action 4: ', JSON.stringify(action4, null, 2));
    console.log("State: ", JSON.stringify(await stateStore.loadState(action4.executionId), null, 2));

    const action5 = await flowEngine.flowSubmit('claim/breakdown', {
        executionId: action4.executionId,
        data: {
            "appliance_environment": "utility",
            "appliance_environment_safety": true
        }
    });
    console.log('Action 5: ', JSON.stringify(action5, null, 2));
    console.log("State: ", JSON.stringify(await stateStore.loadState(action5.executionId), null, 2));

    const action6 = await flowEngine.flowSubmit('claim/breakdown', {
        executionId: action5.executionId,
        data: {
            "appliance_serial": "WH123452"
        }
    });
    console.log('Action 6: ', JSON.stringify(action6, null, 2));
    console.log("State: ", JSON.stringify(await stateStore.loadState(action6.executionId), null, 2));

}

main();
